#!/bin/bash


# to maintain cask ....
#     brew update && brew upgrade brew-cask && brew cleanup && brew cask cleanup`


# Install native apps

brew install caskroom/cask/brew-cask
# brew tap caskroom/versions

# daily
# brew cask install spectacle
# brew cask install dropbox
# brew cask install gyazo
# brew cask install 1password
# brew cask install rescuetime
# brew cask install flux
brew cask install alfred
brew cask install paragon-ntfs
brew cask install parallels
brew cask install bartender

# dev
brew cask install iterm2
brew cask install sublime-text
brew cask install phpstorm
brew cask install datagrip
brew cask install pycharm
# brew cask install imagealpha
# brew cask install imageoptim

# fun
# brew cask install limechat
# brew cask install miro-video-converter
# brew cask install horndis               # usb tethering

# browsers
# brew cask install google-chrome-canary
brew cask install firefoxnightly
# brew cask install webkit-nightly
# brew cask install chromium
# brew cask install torbrowser

# less often
# brew cask install disk-inventory-x
# brew cask install screenflow
# brew cask install vlc
# brew cask install gpgtools
# brew cask install licecap
# brew cask install utorrent
brew cask install postman
brew cask install gimp
brew cask install etcher
brew cask install sketch
brew cask install slack
brew cask install makemkv
brew cask install vnc-server
brew cask install vnc-viewer

# brew cask install spotify
brew cask install plex-media-player
brew cask install plexamp

brew tap caskroom/fonts 
brew cask install font-fira-code